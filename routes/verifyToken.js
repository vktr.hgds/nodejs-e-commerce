const axios = require("axios");

const verifyToken = async (req, res, next) => {
  try {
    const user = await axios({
      method: "post",
      url: "http://localhost:3000/auth/validate_token",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${req.headers["authorization"]}`,
      },
    });

    req.user = user.data.user;
    next();
  } catch (err) {
    res.status(err.response.status).json(err.response.data);
  }
};

const verifyTokenAndAuthorization = (req, res, next) => {
  verifyToken(req, res, () => {
    if (
      req.body.username === req.params.username ||
      req?.user?.ref == "admin"
    ) {
      next();
    } else {
      res.status(403).json();
    }
  });
};

const verifyTokenAndAdmin = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req?.user?.ref == "admin") {
      next();
    } else {
      res.status(403).json();
    }
  });
};

module.exports = {
  verifyToken,
  verifyTokenAndAuthorization,
  verifyTokenAndAdmin,
};
